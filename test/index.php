<?php
require  __DIR__ . '\Classes\Csv.php';
require  __DIR__ . '\Csv.php';
require  __DIR__ . '\default_value.php';
session_start();
$CSV = new Csv;

if (isset( $_SESSION['destiation_dir']))
{
    $data_csv= $CSV->parse_csv_file($_SESSION['destiation_dir']) ;
}

if(isset( $_POST['loading'] ) )
{
    if(isset($_FILES) && $_FILES['inputfile']['error'] == 0)// Проверка загрузики файла
    {
        $destiation_dir = dirname(__FILE__) .'/csv_files/'.$_FILES['inputfile']['name']; // Директория для размещения файла
        $_SESSION['destiation_dir'] = $destiation_dir;
        move_uploaded_file($_FILES['inputfile']['tmp_name'], $destiation_dir ); // Перемещаем файл в желаемую директорию
        echo 'Файл успешно загружен'; // Оповещаем пользователя об успешной загрузке файла
        $CSV = new Csv;
        $data_csv= $CSV->parse_csv_file($destiation_dir) ;
    }
    else echo 'Файл не загружен'; // Оповещаем пользователя о том, что файл не был загружен
}

if (isset($_POST['send_sel']) && (in_array("-1", $_POST)==false)) //если всё выбранно
{
    unset($_POST['send_sel']);
    //Проверка на выбор одинаковых значнеий
    if (!in_array($check, array_count_values ($_POST) ))
    {
        $post_column_number = [];
        $j=0;

        foreach ($_POST as $item => $value)
        {
            $post_column_number[] = $value;
            $j++;
        }

        foreach ($data_csv as $data => $sel_row) //пребор всего массива
        {
            if ($data != 0)//игнорирование первой строки
            {
                //$value_query = []; Если нужен массив c данными для запроса, а не строка
                unset($value_query);
                foreach ($sel_row as $row1 => $cell) //перебор сторки
                {
                    if (in_array($row1, $post_column_number) == true)
                    {
                        if ($cell != null)
                        {
                            $value_query .= "'".$cell."',";
                            //$value_query[$key]=$cell; Если нужен массив c данными для запроса, а не строка
                            //поиск для записа в массив реализую таким образом, иначе происходит сортировка по возрастанию
                            //$key = array_search($row1, $post_column_number);
                            //$value_query[$key] = $cell;
                        }
                        else goto a;
                    }
                }
                $query_val = mb_substr($value_query, 0, -1);
                $connection->query("INSERT INTO $table_for_import_csv (`ID`, `name`, `type`, `field`, `coment`, `number`, `site`) VALUES ($query_val);");
                // $connection->query("INSERT INTO `id13102010_larin`.`test` (`ID`, `name`, `type`, `field`, `coment`, `number`, `site`) VALUES ('$value_query[0]', '$value_query[1]', '$value_query[2]', '$value_query[3]', '$value_query[4]', '$value_query[5]', '$value_query[5]');");
            }
            a:
        }
    }
    else echo "Повтор";
}

?>
<!DOCTYPE html>
    <html>
    <head>
        <title>Загрузка csv</title>
    </head>
    <body>
        <h1>Загрузка файла</h1>
        <form method="post" action="" enctype="multipart/form-data">
            <label for="inputfile">Загрузить</label>
            <input type="file" id="inputfile" name="inputfile"></br></br>
            <input type="submit" name="loading" value="Загрузить">
            </br></br>
            <?php foreach ($default_name_column as $column){
                $column_number=0; //порядковый номер столбца в csv?>
                <span class="title"><?php echo $column?></span>
                <select class="form-control" name="<?php echo $column?>">
                    <option value="-1" selected="selected">Выберите</option>
                    <?php foreach  ($data_csv[0] as $datum){ ?>
                    <option value="<?php echo $column_number++;?>"><?php echo $datum;}?></option>
                </select>
                </br></br>
            <?php }?>
            <input type="submit" name="send_sel" value="Готово">
            <p><a href="show_mas.php">Просмотреть таблицу</a></p>
        </form>
</body>
</html>